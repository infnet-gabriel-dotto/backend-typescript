import { inject } from "inversify";
import { httpGet, BaseHttpController, interfaces, controller, queryParam, requestParam, httpPost, requestBody, httpPut } from "inversify-express-utils";
import TYPES from "../../types";

import { ListarProdutosInterface } from "../../core/usecases/produtos/listar-produtos/listarprodutos.interface";
import { ListarProdutosDto } from "../../presentation/dto/listar-produtos.dto";
import { CriarProdutosDto } from "../../presentation/dto/criar-produtos.dto";
import { CriarProdutosInterface } from "../../core/usecases/produtos/criar-produtos/criarprodutos.interface";

import { AlterarProdutosDto } from "../../presentation/dto/alterar-produtos.dto";
import { DtoValidatorMiddleware } from "../middlewares/dto-validator.middleware";

@controller('/produtos')
export class ProdutosController extends BaseHttpController implements interfaces.Controller {

    private _listarProdutosService: ListarProdutosInterface;
    private _criarProdutosService: CriarProdutosInterface;

    constructor(
        @inject(TYPES.ListarProdutosInterface) listarProdutosUseCase: ListarProdutosInterface,
        @inject(TYPES.CriarProdutosInterface) criarProdutosUseCase: CriarProdutosInterface 
    ) {
        super();
        this._listarProdutosService = listarProdutosUseCase;
        this._criarProdutosService = criarProdutosUseCase;
    }

    @httpGet("/")
    public async lista(
        @queryParam() query: ListarProdutosDto.Query,
    ): Promise<interfaces.IHttpActionResult> {
        
        //todo:recuperar dados da request
        console.log(query);

        //todo: invocar usecase
        const resultado: any[] = this._listarProdutosService.execute({});

        return this.json(resultado);

    }
    
    @httpGet("/:id")
    public async buscaPorId(
        @requestParam('id') id: string,
    ): Promise<interfaces.IHttpActionResult> {
        
        try {
            console.log(id);
            
            return this.json({
                id: 1,
                descricao: 'Produto 1',
                status: 'inativo'
            });

        } catch (error) {
            
            if (error.name == 'BusinessError') {

                return this.badRequest(error.message);
            }
                
            return this.internalServerError(error.message);
        }
    }

    @httpPost(
        "/", 
        DtoValidatorMiddleware(CriarProdutosDto.Body, "body"),
    )
    public async cria(
        @requestBody() body: CriarProdutosDto.Body,
    ): Promise<interfaces.IHttpActionResult> {
        
        const result = this._criarProdutosService.execute({
            preco: body.preco,
            descricao: body.descricao
        });

        return this.json(result);    
    }    

    @httpPut(
        "/:id",
        DtoValidatorMiddleware(AlterarProdutosDto.Params, "params"),
        DtoValidatorMiddleware(AlterarProdutosDto.Body, "body"),
    )
    public async altera(
        @requestParam(':id') params: string,
        @requestBody() body: AlterarProdutosDto.Body,
    ): Promise<interfaces.IHttpActionResult> {
        
        console.log(params);
        console.log(body);
        
        return this.json({
            mensagem: 'sucesso',
            data: {
                id: 'string',
                descricao: 'string',
                preco: 'number',
                status: 'ativo'
            }
        })
    }
}