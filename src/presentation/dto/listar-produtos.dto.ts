import { IsNotEmpty, IsString } from "class-validator";

export namespace ListarProdutosDto {

    export class Query {

        @IsString()
        @IsNotEmpty()
        status: string

    }
}
