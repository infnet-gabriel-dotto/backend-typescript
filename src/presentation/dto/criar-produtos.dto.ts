import { IsNotEmpty, IsString } from "class-validator";

export namespace CriarProdutosDto {

    export class Body {

        @IsString()
        @IsNotEmpty()
        descricao: string;

        @IsString()
        @IsNotEmpty()
        preco: number;

        // @IsString()
        // @IsNotEmpty()
        // status: string;
    }
}
