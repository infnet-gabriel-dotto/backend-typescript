import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export namespace AlterarProdutosDto {

    export class Params {
        @IsString()
        id: string;
    }

    export class Body {
        @IsString()
        @IsNotEmpty()
        descricao: string;
        
        @IsString()
        @IsNotEmpty()
        status: string;

        @IsString()
        @IsNotEmpty()
        preco: number;
    }
}
