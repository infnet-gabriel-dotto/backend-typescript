export class ProdutosEntity {

    public id: number;
    public descricao: string;
    public status: string;
    public preco: number;

    constructor(
        produtoId: number,
        descricao: string,
        preco: number,
    ) {        
        this.id = produtoId;
        this.descricao = descricao;
        this.preco = preco;
        this.status = this.getProdutoStatus();

    }

    private getProdutoStatus() : string {
        return "status_mock";
    }

    static build(
        produtoId: number,
        descricao: string,
        preco: number,
    ): ProdutosEntity {
        return new ProdutosEntity(
            produtoId,
            descricao,
            preco
        );
    }

}
