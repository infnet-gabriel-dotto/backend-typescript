import { ProdutosEntity } from "@core/entity/produto.entity";

export class CriarProdutosUseCaseParams {
    descricao: string;
    preco: number;                
}

export interface CriarProdutosInterface {
    
    execute(model: CriarProdutosUseCaseParams): ProdutosEntity;
}