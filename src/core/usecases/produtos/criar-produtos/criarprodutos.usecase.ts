import "reflect-metadata";
import { inject, injectable } from "inversify";
import { CriarProdutosUseCaseParams, CriarProdutosInterface } from "./criarprodutos.interface";
import { ProdutosEntity } from "@core/entity/produto.entity";
import TYPES from "../../../../types";
import { ProdutoRepositoryInterface } from "../../../providers/data/produtos-repository.interface";

@injectable()
export class CriarProdutosUseCase implements CriarProdutosInterface {
    
    private _produtosRepository: ProdutoRepositoryInterface;

    constructor(
        @inject(TYPES.ProdutoRepositoryInterface) produtosRepository: ProdutoRepositoryInterface 
    ) {
        // super();
        this._produtosRepository = produtosRepository;
    }

    execute(model: CriarProdutosUseCaseParams): ProdutosEntity {

        const cursoFromDb = this._produtosRepository.search({
            descricao: model.descricao
        });
        
        if (!cursoFromDb)
            throw new Error("");            

        const result = this._produtosRepository.create({
            preco: model.preco,
            descricao: model.descricao
        });

        return result;
    }
}