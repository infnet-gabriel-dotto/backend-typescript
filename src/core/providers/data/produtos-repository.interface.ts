import { ProdutosEntity } from "@core/entity/produto.entity";

export type ProdutoRespositorySearchParams = {
    descricao?: string;    
}

export type ProdutoRespositoryCreateParams = {
    preco: number;    
    descricao: string;    
}

export interface ProdutoRepositoryInterface {
    
    search(model: ProdutoRespositorySearchParams): ProdutosEntity[];

    create(model: ProdutoRespositoryCreateParams): ProdutosEntity;
}