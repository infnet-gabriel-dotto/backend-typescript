import { ProdutoRespositorySearchParams, ProdutoRespositoryCreateParams, ProdutoRepositoryInterface } from "../../../core/providers/data/produtos-repository.interface";
import { injectable } from "inversify";
import { ProdutosEntity } from "../../../core/entity/produto.entity";

const data = [];

@injectable()
export class ProdutoRepository implements ProdutoRepositoryInterface {

    create(model: ProdutoRespositoryCreateParams): ProdutosEntity {

        const id = 0;

        const dataModel = {
            id,
            preco_produto: model.preco,
            descricao_produto: model.descricao
        }

        data.push(dataModel);

        return ProdutosEntity.build(
            dataModel.id,
            dataModel.descricao_produto,
            dataModel.preco_produto
        );
    }

    search(model: ProdutoRespositorySearchParams): ProdutosEntity[] {
        throw new Error("Method not implemented.");
    } 
}